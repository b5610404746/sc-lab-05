package library;

public class Main {
		
	public static void main(String[] args) {
		Library lib = new Library();
		Student student = new Student("Tuator", "Lover", "5610404738");
		Instructor instructor = new Instructor("Usa","Sammapan","D14");
		Staff staff = new Staff("Max","cs","191");
		
		Book book1 = new Book("Big java","1000","Ownice");
		Book book2 = new Book("Travel","500","J. san");
		Book book3 = new Book("Animal","650","Belh pheger");
		ReferenceBook refBook = new ReferenceBook("Deadly","500","Coph");
		
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addBook(book3);
		lib.addRefBook(refBook);
		
		System.out.println(staff.getName());
		
		System.out.println(lib.bookCount());
		lib.borrowBook(student,book1);
		System.out.println(lib.bookCount());
		System.out.print(book1.getBorrowBook());
		System.out.println(student.getName());
		
		System.out.println(lib.bookCount());
		lib.teacBorrowBook(instructor,refBook);
		System.out.println(lib.bookCount());
		System.out.print(refBook.getBorrowBook());
		System.out.println(instructor.getName());

	}

}
